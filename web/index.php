<?php

require_once(__DIR__."/../vendor/autoload.php");
// Load the dotenv
require_once(__DIR__."/../app/bootstrap.php");

use Adi\MyApplication;

// Initialize the application
$app = new MyApplication();
// Set debug to true in development
$app['debug'] = true;

// Setup the providers here ---
$app->register(new Silex\Provider\TwigServiceProvider(), array(
  'twig.path' => __DIR__.'/views'
));

$app->register(new Silex\Provider\MonologServiceProvider(), array(
  'monolog.logfile' => 'php://stderr'
));

$dbopts = parse_url(getenv('DATABASE_URL'));
$app->register(new Silex\Provider\DoctrineServiceProvider(), array(
    'db.options' => array(
        'driver' => 'pdo_pgsql',
        'user' => $dbopts['user'],
        'password' => $dbopts['pass'],
        'host' => $dbopts['host'],
        'port' => $dbopts['port'],
        'dbname' => ltrim($dbopts["path"],'/'),
        'charset' => 'utf-8'
    )
));
$app['db']->setFetchMode(PDO::FETCH_OBJ);

$app->register(new Silex\Provider\FormServiceProvider());
$app->register(new Silex\Provider\ValidatorServiceProvider());
$app->register(new Silex\Provider\UrlGeneratorServiceProvider());
$app->register(new Silex\Provider\TranslationServiceProvider(), array(
    'locale_fallback' => array('en'),
    'translator.domains' => array(),
));
// --- End of providers ---

// Setup the controllers here ---
$app->mount('/', new Adi\Controllers\IndexController());
// --- End of controllers ---

$app->run();
