<?php
namespace Adi\Models;

use Silex\Application;

class TestModel
{
    protected $db;

    public function __construct(Application $app) {
        $this->db = $app['db'];
    }

    public function showAllData() {
        try {
            $sql = 'SELECT name FROM test_table';
            $conn = $this->db->prepare($sql);
            $conn->execute();

            $post = $conn->fetchAll();
        } catch (Exception $e) {
            die($e);
        }

        return $post;
    }
}
