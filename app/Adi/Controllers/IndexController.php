<?php
namespace Adi\Controllers;

use Adi\Models\TestModel;

use Silex\Application;
use Silex\ControllerProviderInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Validator\Constraints as Assert;

class IndexController implements ControllerProviderInterface
{
    public function connect(Application $app) {
        // $app['monolog']->addDebug('logging output');
        $index = $app['controllers_factory'];

        $index->match('/', function(Request $request) use ($app) {
            // return $app['twig']->render('index.twig');
            // $model = new TestModel($app);

            $data = array(
                'name' => 'Your name',
                'email' => 'Your email',
                'date' => new \DateTime('now'),
            );

            $form = $app->form($data)
                ->add('name', 'text', array(
                    'constraints' => array(new Assert\NotBlank(), new Assert\Length(array('min' => 5)))
                ))
                ->add('email')
                ->add('dueDate', 'date')
                ->add('gender', 'choice', array(
                    'choices' => array(1 => 'male', 2 => 'female'),
                    'expanded' => true
                ))
                ->getForm();

            if ($request->isMethod('POST')) {

                $form->submit($request->request->get($form->getName()));

                if ($form->isValid()) {
                    $data = $form->getData();

                    return $app->redirect($app->path('home'));
                }
            }

            // $form->handleRequest($request);

            // if ($form->isValid()) {
            //     $data = $form->getData();

            //     return $app->redirect($app->path('home'));
            // }

            return $app['twig']->render('index.twig', array(
                'form' => $form->createView(),
            ));

            // return $app['twig']->render('index.twig', array(
            //     'posts' => $model->showAllData()
            // ));
        })->bind('home');

        return $index;
    }
}
